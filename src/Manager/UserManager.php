<?php

namespace App\Manager;

use App\Entity\Group;
use App\Entity\User;
use App\Form\UserType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserManager extends BaseManger
{

    /**
     * fetch user list
     *
     * @param Request $request
     * @return Mixed
     */
    public function getList(Request $request){
        $userManager = $this->getRepository(User::class);
        $params     = $request->query->all();
        if(isset($params['group'])){
            $repository = $this->getRepository(Group::class);
            $oGroup     = $repository->findOneById($params['group']);
            return $oGroup ? $oGroup->getMembers() : [];
        }

        return $userManager->findAll();
    }
    /**
     * Handle and persist new user
     *
     * @param Request $request
     * @return Mixed
     */
    public function createUser(Request $request){
        $user = new User();
        $form = $this->formBuilder->create(UserType::class, $user);
        $form->handleRequest($request);
        
        
        $format = $request->getRequestFormat();
        if($format == 'json'){
            $form->submit($request->request->all()); 
        }

        $response = ['user' => $user, 'form' => $form];
        if($form->isSubmitted()) {
            if($this->emailExist($user->getEmail())){
                return $response + ['message' => 'Email already taken'];
            }
            return $this->save($user);
        }
        return $response;
    }

    /**
     * Create new group
     *
     * @param Request $request
     * @return void
     */
    public function updateUser(Request $request, User $user){

        $form = $this->formBuilder->create(UserType::class, $user);
        $form->handleRequest($request);

        $format = $request->getRequestFormat();
        if($format == 'json'){
            $form->submit($request->request->all()); 
        }
        
        $response = ['user' => $user, 'form' => $form];

        /** check if user is admin */
        if($user->getId() === 1){
            return $response + ['message' => 'Action not permitted'];
        }

        /*** check if new email exis */
        $exist =   $this->emailExist($user->getEmail());          
        if($exist && $user->getId() != $exist->getId() ){
            return $response + ['message' => 'Email already taken'];
        }

        if ($form->isSubmitted()) {
            return $this->save($user);
        }
        return $response;
    }

    /**
     * Create new group
     *
     * @param User $user
     * @return int
     */
    public function deleteUser(User $user){
        /** check if user is admin */
        if($user->getId() === 1){
            return ['message' => 'Action not permitted'];
        }
        $this->remove($user);
        return Response::HTTP_OK; 
    }

    public function getUser(Request $request){

    }

    /**
     * Undocumented function
     *
     * @param User $group
     * @return void
     */
    public function getAvailableGroup(User $user){
        /**
         * @var UserRepository
         */
        $repository = $this->getRepository(Group::class);
        return $repository->findAvailableGroups($user);
    }

    /**
     * Add member to the group
     *
     * @param User $group
     * @param [type] $members
     * @return void
     */
    public function addGroups(User $user, $groups){
         /**
         * @var GroupRepository
         */
        $groupManager =$this->getRepository(Group::class);
        foreach ($groups as $gid) {
           $oGroup = $groupManager->findOneById($gid);
           if($oGroup){
              $user->addGroupList($oGroup);
           }
        }
        return $this->save($user);
    }

    /**
     * Remove an user with uid id in the group
     *
     * @param [type] $group
     * @param [type] $uid
     * @return void
     */
    public function removeGroup($user, $gid){
       /**
         * @var GroupRepository
         */
        $groupManager = $this->getRepository(Group::class);
        $oGroup = $groupManager->findOneById($gid);
        if($oGroup){
            $user->removeGroupList($oGroup);
        }
        return $this->save($user);
    }

    private function emailExist($email){
        $userManager = $this->getRepository(User::class);
        return $userManager->findOneByEmail($email);

    }
}
