<?php 
namespace App\Manager;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\Form\FormFactoryBuilder;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;

class BaseManger
{   
    /**
     * Undocumented variable
     *
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    protected $formBuilder;
    
    /**
     * Undocumented function
     *
     * @param EntityManagerInterface $entityManager
     * @param FormFactoryInterface $formBuilder
     */
    public function __construct(EntityManagerInterface $entityManager, FormFactoryInterface $formBuilder)
    {
        $this->entityManager = $entityManager;
        $this->formBuilder   = $formBuilder;
    }

    /**
     *
     * @param [type] $entity
     * @return void
     */
    public function save($entity){
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
        return HttpFoundationResponse::HTTP_CREATED;
    }
    
    /**
     * Undocumented function
     *
     * @param [type] $entity
     * @return void
     */
    public function remove($entity){
        $this->entityManager->remove($entity);
        $this->entityManager->flush();
        return HttpFoundationResponse::HTTP_CREATED;
    }
    
    /**
     * Get EntityRepository
     *
     * @param  String $class
     * @return EntityRepository
     */
    public function getRepository($class){
        return $this->entityManager->getRepository($class);
    }
  
}