<?php 
namespace App\Manager;

use App\Entity\Group;
use App\Entity\User;
use App\Form\GroupType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GroupManager extends BaseManger
{   
    /**
     * Fetch user group list
     *
     * @param Request $request
     * @return Mixed
     */
    public function getList(Request $request)
    {
        $repository = $this->getRepository(Group::class);
        $params     = $request->query->all();
        if(isset($params['user'])){
            $userRepository =  $this->getRepository(User::class);
            $oUser          =  $userRepository->findOneById($params['user']);
            return $oUser ? $oUser->getGroupList() : [];
        }
        return $repository->findAll();
    }

    /**
     * Create new group
     *
     * @param Request $request
     * @return void
     */
    public function createGroup(Request $request){
        $group  = new Group();
        $form   = $this->formBuilder->create(GroupType::class, $group);
        $form->handleRequest($request);  
        
        $format = $request->getRequestFormat();
        if($format == 'json'){
            $form->submit($request->request->all()); 
        }

        if($form->isSubmitted()){
           return $this->save($group);
        }

        return ['group' => $group, 'form' => $form];
    }

    /**
     * Check and delete group if empty
     *
     * @param Request $request
     * @return void
     */
    public function deleteGroup(Group $group){
       $members = $group->getMembers();
        if(!$members->isEmpty()){
            return [
                'error'   => Response::HTTP_UNAUTHORIZED,
                'message' => 'Action not permitted. The user group contains a member.'
            ];
        }
        $this->remove($group);
        return Response::HTTP_OK; 
    }

    /**
     * Create new group
     *
     * @param Request $request
     * @return void
     */
    public function updateGroup(Request $request, Group $group){

        $form = $this->formBuilder->create(GroupType::class, $group);
        $form->handleRequest($request);
        
        $format = $request->getRequestFormat();
        if($format == 'json'){
            $form->submit($request->request->all()); 
        }

        if ($form->isSubmitted()){
           return $this->save($group);
        }

        return ['group' => $group, 'form' => $form];
    }

    /**
     * Undocumented function
     *
     * @param Group $group
     * @return void
     */
    public function getAvailableMember(Group $group){
        /**
         * @var UserRepository
         */
        $repository = $this->entityManager->getRepository(User::class);
        return $repository->findAvailableMembers($group);
    }
    /**
     * Add member to the group
     *
     * @param Group $group
     * @param [type] $members
     * @return void
     */
    public function addMembers(Group $group, $members){
        /**
         * @var UserRepository
         */
        $userManager = $this->entityManager->getRepository(User::class); 
        foreach ($members as $mid) {
           $oMember = $userManager->findOneById($mid);
           if($oMember){
              $group->addMember($oMember);
           }
        }
        return $this->save($group);
    }

    /**
     * Remove an user with uid id in the group
     *
     * @param [type] $group
     * @param [type] $uid
     * @return void
     */
    public function removeUser($group, $uid){
        /**
         * @var UserRepository
         */
        $userManager = $this->entityManager->getRepository(User::class); 
        $oMember = $userManager->findOneById($uid);
        if($oMember){
            $group->removeMember($oMember);
        }
        return $this->save($group);
    }
}