<?php

namespace App\Controller\Admin\Group;

use App\Manager\GroupManager;
use App\Repository\groupRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/group")
 */
class ListAction extends AbstractController
{
    /**
     * @Route("/", name="app_admin_group_index", methods={"GET"})
     */
    public function index(Request $request, GroupManager $groupManager): Response
    {
        return $this->render('admin/group/index.html.twig', [
            'groups' => $groupManager->getList($request),
        ]);
    }
}
