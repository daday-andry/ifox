<?php

namespace App\Controller\Admin\Group;

use App\Entity\Group;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/group/{id}/show", name="app_admin_group_show")
 */
class ShowAction extends AbstractController
{
   
    public function __invoke(Group $group){
        return $this->render('admin/group/show.html.twig', [
            'group' => $group,
        ]);
    }
}
