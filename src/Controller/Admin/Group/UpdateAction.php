<?php

namespace App\Controller\Admin\Group;

use App\Entity\Group;
use App\Manager\GroupManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/group/{id}/edit", name="app_admin_group_edit")
 */
class UpdateAction extends AbstractController
{
    /**
     * Undocumented function
     *
     * @param Request $request
     * @param Group $group
     * @param GroupManager $groupManager
     * @return void
     */
    public function __invoke(Request $request, Group $group,  GroupManager $groupManager){
        $response = $groupManager->updateGroup($request, $group);
    
        if($response === Response::HTTP_CREATED ){
            return $this->redirectToRoute('app_admin_group_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin/group/edit.html.twig', [
            'group' => $response['group'],
            'form' => $response['form'],
        ]);
    }
}
