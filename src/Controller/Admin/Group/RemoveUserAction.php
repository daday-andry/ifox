<?php

namespace App\Controller\Admin\Group;

use App\Entity\Group;
use App\Manager\GroupManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/group/{id}/remove-user", name="app_admin_group_remove_user", methods={"POST"})
 */
class RemoveUserAction extends AbstractController
{
    /**
     * Undocumented function
     *
     * @param Request $request
     * @param Group $group
     * @param GroupManager $groupManager
     * @return void
     */
    public function __invoke(Request $request, Group $group,  GroupManager $groupManager){
        
        if ($this->isCsrfTokenValid('delete'.$group->getId(), $request->request->get('_token'))) {
           $uid = $request->request->get('_user');
           if($uid){
               $response = $groupManager->removeUser($group, $uid);
           }

           $this->addFlash('notice', 'User removed from this group');
           return $this->redirectToRoute('app_admin_group_show', [ 'id' => $group->getId()]);  
        }

        return $this->redirectToRoute('app_admin_group_index', [], Response::HTTP_FORBIDDEN);
    }
}
