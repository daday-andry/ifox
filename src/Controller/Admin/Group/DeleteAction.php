<?php

namespace App\Controller\Admin\Group;

use App\Entity\Group;
use App\Manager\GroupManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/group/{id}/delete", name="app_admin_group_delete", methods={"POST"})
 */
class DeleteAction extends AbstractController
{
    /**
     * Undocumented function
     *
     * @param Request $request
     * @param Group $group
     * @param GroupManager $groupManager
     * @return void
     */
    public function __invoke(Request $request, Group $group,  GroupManager $groupManager){
        $response = $groupManager->updateGroup($request, $group);
    
        if ($this->isCsrfTokenValid('delete'.$group->getId(), $request->request->get('_token'))) {
           $response = $groupManager->deleteGroup($group);
           if(Response::HTTP_OK == $response){
               return $this->redirectToRoute('app_admin_group_index', []);
           }
           $this->addFlash('error', $response['message']);
           return $this->redirectToRoute('app_admin_group_show', [ 'id' => $group->getId()]);
        }

        return $this->redirectToRoute('app_admin_group_index', [], Response::HTTP_FORBIDDEN);
    }
}
