<?php

namespace App\Controller\Admin\Group;

use App\Manager\GroupManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AddAction extends AbstractController
{
    /**
     * @Route("/admin/group/new", name="app_admin_group_new")
     */
    public function __invoke(Request $request, GroupManager $groupManager){
        $response = $groupManager->createGroup($request);
    
        if($response === Response::HTTP_CREATED ){
            return $this->redirectToRoute('app_admin_group_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin/group/new.html.twig', [
            'group' => $response['group'],
            'form' => $response['form'],
        ]);
    }
}
