<?php

namespace App\Controller\Admin\Group;

use App\Entity\Group;
use App\Manager\GroupManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Console\Helper\Helper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/group/{id}/members", name="app_admin_group_add_member")
 */
class AddMemberAction extends AbstractController
{
    /**
     * Undocumented variable
     *
     * @var GroupManager
     */
    private $manager;

    /**
     * Undocumented function
     *
     * @param GroupManager $manager
     */
    public function __construct(GroupManager $manager)
    {
        $this->manager = $manager;
    }
   
    public function __invoke(Request $request, Group $group){
        $members  = $this->manager->getAvailableMember($group);
        $response = $this->handleSubmitMembers($request, $group);
        
        if($response == Response::HTTP_CREATED){
            return $this->redirectToRoute('app_admin_group_show', ['id'=> $group->getId()]);
        }

        return $this->render('admin/group/members.html.twig', [
            'group'   => $group,
            'members' => $members
        ]);
    }

    public function handleSubmitMembers(Request $request, $group){
        $members = $request->request->get('members', false);
        if(!$members){
            return false;
        }

        return $this->manager->addMembers($group, $members);
    }
}
