<?php

namespace App\Controller\Admin\User;

use App\Manager\UserManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AddAction extends AbstractController
{
    /**
     * @Route("/admin/user/new", name="app_admin_user_new")
     */

    public function __invoke(Request $request, UserManager $userManager){
        $response = $userManager->createUser($request);
    
        if($response === Response::HTTP_CREATED ){
            return $this->redirectToRoute('app_admin_user_index', [], Response::HTTP_SEE_OTHER);
        }

        if(isset($response['message'])){
            $this->addFlash('error', $response['message']);
        }

        return $this->renderForm('admin/user/new.html.twig', [
            'user' => $response['user'],
            'form' => $response['form'],
        ]);
    }
}
