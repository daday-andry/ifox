<?php

namespace App\Controller\Admin\User;

use App\Entity\User;
use App\Manager\GroupManager;
use App\Manager\UserManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Console\Helper\Helper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/user/{id}/groups", name="app_admin_user_add_group")
 */
class AddGroupAction extends AbstractController
{
    /**
     * Undocumented variable
     *
     * @var UserManager
     */
    private $manager;

    /**
     * Undocumented function
     *
     * @param UserManager $manager
     */
    public function __construct(UserManager $manager)
    {
        $this->manager      = $manager;
    }
   
    public function __invoke(Request $request, User $user){
        $groups  = $this->manager->getAvailableGroup($user);
        $response = $this->handleSubmitGroups($request, $user);
        
        if($response == Response::HTTP_CREATED){
            return $this->redirectToRoute('app_admin_user_show', ['id'=> $user->getId()]);
        }

        return $this->render('admin/user/groups.html.twig', [
            'user'   => $user,
            'groups' => $groups
        ]);
    }

    public function handleSubmitGroups(Request $request, $user){
        $groups = $request->request->get('groups', false);
        if(!$groups){
            return false;
        }

        return $this->manager->addGroups($user, $groups);
    }
}
