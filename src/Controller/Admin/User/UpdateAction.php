<?php

namespace App\Controller\Admin\User;

use App\Entity\User;
use App\Manager\GroupManager;
use App\Manager\UserManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/user/{id}/edit", name="app_admin_user_edit")
 */
class UpdateAction extends AbstractController
{
    /**
     * Undocumented function
     *
     * @param Request $request
     * @param User $user
     * @param UserManager $userManager
     * @return void
     */
    public function __invoke(Request $request, User $user,  UserManager $userManager){
        $response = $userManager->updateUser($request, $user);
    
        if($response === Response::HTTP_CREATED ){
            return $this->redirectToRoute('app_admin_user_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin/user/edit.html.twig', [
            'user' => $response['user'],
            'form' => $response['form'],
        ]);
    }
}
