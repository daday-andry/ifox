<?php

namespace App\Controller\Admin\User;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/user/{id}/show", name="app_admin_user_show")
 */
class ShowAction extends AbstractController
{
   
    public function __invoke(User $user){
        return $this->render('admin/user/show.html.twig', [
            'user' => $user,
        ]);
    }
}
