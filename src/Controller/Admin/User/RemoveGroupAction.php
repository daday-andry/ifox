<?php

namespace App\Controller\Admin\User;

use App\Entity\Group;
use App\Entity\User;
use App\Manager\GroupManager;
use App\Manager\UserManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/user/{id}/remove-group", name="app_admin_user_remove_group", methods={"POST"})
 */
class RemoveGroupAction extends AbstractController
{
    /**
     * Undocumented function
     *
     * @param Request $request
     * @param Group $group
     * @param GroupManager $groupManager
     * @return void
     */
    public function __invoke(Request $request, User $user,  UserManager $userManager){
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
           $gid = $request->request->get('_group');
           if($gid){
               $response = $userManager->removeGroup($user, $gid);
           }

           $this->addFlash('notice', 'Group removed from this user');
           return $this->redirectToRoute('app_admin_user_show', [ 'id' => $user->getId()]);  
        }

        return $this->redirectToRoute('app_admin_user_index', [], Response::HTTP_FORBIDDEN);
    }
}
