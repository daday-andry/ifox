<?php

namespace App\Controller\Admin\User;

use App\Entity\User;
use App\Manager\GroupManager;
use App\Manager\UserManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/user/{id}/delete", name="app_admin_user_delete", methods={"POST"})
 */
class DeleteUser extends AbstractController
{
    /**
     * Undocumented function
     *
     * @param Request $request
     * @param User $user
     * @param UserManager $userManager
     * @return void
     */
    public function __invoke(Request $request, User $user,  UserManager $userManager){
        $response = $userManager->updateUser($request, $user);
    
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
           $response = $userManager->deleteUser($user);
           return $this->redirectToRoute('app_admin_user_index', []);
        }

        return $this->redirectToRoute('app_admin_user_index', [], Response::HTTP_FORBIDDEN);
    }
}
