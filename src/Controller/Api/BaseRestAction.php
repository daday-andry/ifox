<?php
/**
 * @ Author: Daday ANDRY
 * @ Create Time: 2022-04-12 16:47:54
 * @ Modified by: Daday ANDRY
 * @ Modified time: 2022-04-12 17:09:36
 * @ Description:
 */
namespace App\Controller\Api;

use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;


class BaseRestAction extends AbstractFOSRestController
{
    /**
     * Undocumented function
     *
     * @param  Array $response
     * @param  Array $group
     * @return Response
     */
    public function renderResetView(Array $response, Array $group){
        $context = new Context();
        $context->setGroups($group);
        $view = $this->view($response);
        $view->setContext($context);
        return  $this->handleView($view);
    }
}
