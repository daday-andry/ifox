<?php
/**
 * @ Author: Daday ANDRY
 * @ Create Time: 2022-04-12 16:01:16
 * @ Modified by: Daday ANDRY
 * @ Modified time: 2022-04-12 23:15:47
 * @ Description:
 */
namespace App\Controller\Api\User;

use App\Controller\Api\BaseRestAction;
use App\Manager\UserManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

/**
 * @Route("/api/v1")
 */
class GetListAction extends BaseRestAction
{
    /**
     * @Route("/user", name="api_user_list", methods={"GET"})
     * @OA\Response(response=200, description="Return user list")
     * @OA\Parameter(name="group", in="query", description="Group ID", @OA\Schema(type="int"))
     * 
     * @param  Request $request
     * @param  UserManager $userManager
     * @return Response
     */
    public function __invoke(Request $request, UserManager $userManager)
    {   
        $response = $userManager->getList($request);
        return $this->renderResetView(['users' => $response ], ['list']);    
    }
}
