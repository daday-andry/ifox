<?php
/**
 * @ Author: Daday ANDRY
 * @ Create Time: 2022-04-12 16:01:16
 * @ Modified by: Daday ANDRY
 * @ Modified time: 2022-04-12 22:58:33
 * @ Description:
 */
namespace App\Controller\Api\User;

use App\Controller\Api\BaseRestAction;
use App\Entity\User;
use App\Manager\UserManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use OpenApi\Annotations\Response;
use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;
use Nelmio\ApiDocBundle\Annotation\Security;

/**
 * @Route("/api/v1")
 */
class DeleteAction extends BaseRestAction
{
    /**
     * @Route("/user/{id}", name="api_user_delete", methods={"DELETE"})
     * @OA\Response(response=200, description="Delete user")
     * @Security(name="Bearer")
     * 
     * @param  Request      $request
     * @param  UserManager $userManager
     * @return Response
     */
    public function __invoke(User $user, UserManager $manager)
    {   
        $response         = $manager->deleteUser($user);
        $success          = HttpFoundationResponse::HTTP_OK == $response;
        $data['success']  = $success;
        $data['message']  = $success ? 'user deleted successfull' : $response['message'];
        return $this->renderResetView($data,['default']);    
    }
}
