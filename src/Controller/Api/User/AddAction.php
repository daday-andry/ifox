<?php
/**
 * @ Author: Daday ANDRY
 * @ Create Time: 2022-04-12 16:01:16
 * @ Modified by: Daday ANDRY
 * @ Modified time: 2022-04-12 22:40:36
 * @ Description:
 */
namespace App\Controller\Api\User;

use App\Controller\Api\BaseRestAction;
use App\Manager\UserManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use OpenApi\Annotations\Response;
use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;
use Nelmio\ApiDocBundle\Annotation\Security;

/**
 * @Route("/api/v1")
 */
class AddAction extends BaseRestAction
{
    /**
     * @Route("/user", name="api_user_new", methods={"POST"})
     * @OA\Response(response=200, description="Add new user")
     * @OA\Parameter(name="email",     in="query", description="User email",      @OA\Schema(type="string"))
     * @OA\Parameter(name="firstName", in="query", description="User firstName",  @OA\Schema(type="string"))
     * @OA\Parameter(name="lastName",  in="query", description="User lastName",   @OA\Schema(type="string"))
     * @OA\Parameter(name="phone",     in="query", description="User phone",      @OA\Schema(type="string"))
     * @OA\Parameter(name="password",  in="query", description="User password",   @OA\Schema(type="string"))
     * @Security(name="Bearer")
     * 
     * @param  Request      $request
     * @param  UserManager $userManager
     * @return Response
     */
    public function __invoke(Request $request, UserManager $manager)
    {   
        $response = $manager->createUser($request);
        $success  = $response == HttpFoundationResponse::HTTP_CREATED;
        
        if(!$success){
            return $this->renderResetView([
                'success' => false,
                'message' => $response['message']
            ],['default']);    
        }

        return $this->renderResetView([
            'success' => true
        ],['default']);

    }
}
