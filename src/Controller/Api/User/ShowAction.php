<?php
/**
 * @ Author: Daday ANDRY
 * @ Create Time: 2022-04-12 16:01:16
 * @ Modified by: Daday ANDRY
 * @ Modified time: 2022-04-12 22:55:55
 * @ Description:
 */
namespace App\Controller\Api\User;
use App\Controller\Api\BaseRestAction;
use App\Entity\User;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

/**
 * @Route("/api/v1")
 */
class ShowAction extends BaseRestAction
{
    /**
     * @Route("/user/{id}", name="api_user_show", methods={"GET"})
     * @OA\Response(response=200, description="Return user detail")
     * 
     * @param  Request      $request
     * @param  UserManager $userManager
     * @return Response
     */
    public function __invoke(User $user)
    {   
        return $this->renderResetView(['user' => $user ], ['details']);    
    }
}
