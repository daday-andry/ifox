<?php
/**
 * @ Author: Daday ANDRY
 * @ Create Time: 2022-04-12 16:01:16
 * @ Modified by: Daday ANDRY
 * @ Modified time: 2022-04-12 21:40:53
 * @ Description:
 */
namespace App\Controller\Api\Group;

use App\Controller\Api\BaseRestAction;
use App\Entity\Group;
use App\Manager\GroupManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use OpenApi\Annotations\Response;
use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;
use Nelmio\ApiDocBundle\Annotation\Security;

/**
 * @Route("/api/v1")
 */
class DeleteAction extends BaseRestAction
{
    /**
     * @Route("/group/{id}", name="api_group_delete", methods={"DELETE"})
     * @OA\Response(response=200, description="Delete group")
     * @Security(name="Bearer")
     * 
     * @param  Request      $request
     * @param  GroupManager $userManager
     * @return Response
     */
    public function __invoke(Group $group, GroupManager $manager)
    {   
        $response         = $manager->deleteGroup($group);
        $success          = HttpFoundationResponse::HTTP_OK == $response;
        $data['success']  = $success;
        $data['message']  = $success ? 'groupe deleted successfull' : $response['message'];
        return $this->renderResetView($data,['default']);    
    }
}
