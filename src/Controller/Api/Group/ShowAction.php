<?php
/**
 * @ Author: Daday ANDRY
 * @ Create Time: 2022-04-12 16:01:16
 * @ Modified by: Daday ANDRY
 * @ Modified time: 2022-04-12 21:46:35
 * @ Description:
 */
namespace App\Controller\Api\Group;

use App\Controller\Api\BaseRestAction;
use App\Entity\Group;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

/**
 * @Route("/api/v1")
 */
class ShowAction extends BaseRestAction
{
    /**
     * @Route("/group/{id}", name="api_group_show", methods={"GET"})
     * @OA\Response(response=200, description="Return group detail")
     * 
     * @param  Request      $request
     * @param  GroupManager $userManager
     * @return Response
     */
    public function __invoke(Group $group)
    {   
        return $this->renderResetView(['group' => $group ], ['details']);    
    }
}
