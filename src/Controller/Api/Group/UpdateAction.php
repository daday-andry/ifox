<?php
/**
 * @ Author: Daday ANDRY
 * @ Create Time: 2022-04-12 16:01:16
 * @ Modified by: Daday ANDRY
 * @ Modified time: 2022-04-12 21:35:55
 * @ Description:
 */
namespace App\Controller\Api\Group;

use App\Controller\Api\BaseRestAction;
use App\Entity\Group;
use App\Manager\GroupManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use OpenApi\Annotations\Response;
use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;
use Nelmio\ApiDocBundle\Annotation\Security;

/**
 * @Route("/api/v1")
 */
class UpdateAction extends BaseRestAction
{
    /**
     * @Route("/group/{id}", name="api_group_update", methods={"PUT"})
     * @OA\Response(response=200, description="Update user group")
     * @OA\Parameter(name="name",        in="query", description="Group name",         @OA\Schema(type="string"))
     * @OA\Parameter(name="description", in="query", description="Group description",  @OA\Schema(type="string"))
     * @Security(name="Bearer")
     * 
     * @param  Request      $request
     * @param  GroupManager $userManager
     * @return Response
     */
    public function __invoke(Request $request, Group $group, GroupManager $manager)
    {   
        $response = $manager->updateGroup($request, $group);
        $response == HttpFoundationResponse::HTTP_CREATED ? true : false;
        
        return $this->renderResetView([
            'success' => $response
        ],['default']);    
    }
}
