<?php
/**
 * @ Author: Daday ANDRY
 * @ Create Time: 2022-04-12 16:01:16
 * @ Modified by: Daday ANDRY
 * @ Modified time: 2022-04-12 22:17:40
 * @ Description:
 */
namespace App\Controller\Api\Group;

use App\Controller\Api\BaseRestAction;
use App\Entity\Group;
use App\Manager\GroupManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use OpenApi\Annotations\Response;
use Nelmio\ApiDocBundle\Annotation\Security;

/**
 * @Route("/api/v1")
 */
class RemoveUserAction extends BaseRestAction
{
    /**
     * @Route("/group/{id}/remove-user", name="api_group_remove_user", methods={"DELETE"})
     * @OA\Response(response=200, description="Remove user from this group")
     * @OA\Parameter(name="uid", in="query", description="User ID", @OA\Schema(type="int"))
     * @Security(name="Bearer")
     * 
     * @param  Request      $request
     * @param  GroupManager $userManager
     * @return Response
     */
    public function __invoke(Request $request, Group $group, GroupManager $manager)
    {   
        $uid = $request->request->get('uid');
        if(!$uid){
            return $this->renderResetView([
                'success' => false,
                'message' => 'uid is required'
            ],['default']);     
        }
        $response = $manager->removeUser($group, $uid);

        return $this->renderResetView([
            'success' => $response
        ],['default']);    
    }
}