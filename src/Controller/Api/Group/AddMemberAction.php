<?php
/**
 * @ Author: Daday ANDRY
 * @ Create Time: 2022-04-12 16:01:16
 * @ Modified by: Daday ANDRY
 * @ Modified time: 2022-04-12 22:03:42
 * @ Description:
 */
namespace App\Controller\Api\Group;

use App\Controller\Api\BaseRestAction;
use App\Entity\Group;
use App\Manager\GroupManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use OpenApi\Annotations\Response;
use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;
use Nelmio\ApiDocBundle\Annotation\Security;

/**
 * @Route("/api/v1")
 */
class AddMemberAction extends BaseRestAction
{
    /**
     * @Route("/group/{id}/add-user", name="api_group_add_user", methods={"PUT"})
     * @OA\Response(response=200, description="Update user group")
     * @OA\Parameter(name="users",       in="query", description="User ID array", @OA\Schema(type="array", @OA\Items()))
     * @Security(name="Bearer")
     * 
     * @param  Request      $request
     * @param  GroupManager $userManager
     * @return Response
     */
    public function __invoke(Request $request, Group $group, GroupManager $manager)
    {   
        $members = ($request->request->all())['users'] ?? false;
        if(!$members){
            return $this->renderResetView([
                'success' => "false"
            ],['default']);
        }

        $response =  $manager->addMembers($group, $members);
        return $this->renderResetView([
            'success' => $members
        ],['default']);    
    }
}