<?php
/**
 * @ Author: Daday ANDRY
 * @ Create Time: 2022-04-12 16:01:16
 * @ Modified by: Daday ANDRY
 * @ Modified time: 2022-04-12 21:09:49
 * @ Description:
 */
namespace App\Controller\Api\Group;

use App\Controller\Api\BaseRestAction;
use App\Manager\GroupManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use OpenApi\Annotations\Response;
use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;
use Nelmio\ApiDocBundle\Annotation\Security;

/**
 * @Route("/api/v1")
 */
class AddAction extends BaseRestAction
{
    /**
     * @Route("/group", name="api_group_new", methods={"POST"})
     * @OA\Response(response=200, description="Add new group")
     * @OA\Parameter(name="name",        in="query", description="Group name",         @OA\Schema(type="string"))
     * @OA\Parameter(name="description", in="query", description="Group description",  @OA\Schema(type="string"))
     * @Security(name="Bearer")
     * 
     * @param  Request      $request
     * @param  GroupManager $userManager
     * @return Response
     */
    public function __invoke(Request $request, GroupManager $manager)
    {   
        $response = $manager->createGroup($request);
        $response == HttpFoundationResponse::HTTP_CREATED ? true : false;
        
        return $this->renderResetView([
            'success' => $response
        ],['default']);    
    }
}
