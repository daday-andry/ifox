<?php
/**
 * @ Author: Daday ANDRY
 * @ Create Time: 2022-04-12 16:01:16
 * @ Modified by: Daday ANDRY
 * @ Modified time: 2022-04-12 23:03:05
 * @ Description:
 */
namespace App\Controller\Api\Group;

use App\Controller\Api\BaseRestAction;
use App\Manager\GroupManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

/**
 * @Route("/api/v1")
 */
class GetListAction extends BaseRestAction
{
    /**
     * @Route("/group", name="api_group_list", methods={"GET"})
     * @OA\Response(response=200, description="Return group list")
     * @OA\Parameter(name="user", in="query", description="User ID", @OA\Schema(type="int"))
     * 
     * @param  Request      $request
     * @param  GroupManager $userManager
     * @return Response
     */
    public function __invoke(Request $request, GroupManager $manager)
    {   
        $response = $manager->getList($request);
        return $this->renderResetView(['users' => $response ], ['list']);    
    }
}
