<?php
/**
 * @ Author: Daday ANDRY
 * @ Create Time: 2022-04-11 16:31:53
 * @ Modified by: Daday ANDRY
 * @ Modified time: 2022-04-11 16:37:33
 * @ Description:
 */
namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Console\Question\Question;

class AdminUserGeneratorCommand extends Command
{

  protected static $defaultName = 'ifox:create:admin-user';

  private $passwordEncoder;
  private $entityManager;


  public function __construct(UserPasswordEncoderInterface $encoder, EntityManagerInterface $entityManager)
  {
      $this->passwordEncoder = $encoder;
      $this->entityManager = $entityManager;
      parent::__construct();
  }

  protected function configure()
  {
      $this->setDescription('Creates a new ifox admin user.')
          ->setHelp('This command allows you to create a new admin user for ifox user management platforme');
      
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
   
    $helper = $this->getHelper('question');

    $question1 = new Question('User email :', 'admin@ifox.com');
    $question2 = new Question('password   :', '4v3ry5tr0ngp4ssword');

    $email = $helper->ask($input, $output, $question1);
    $password = $helper->ask($input, $output, $question2);
   
    $this->createUser($email, $password);
    $output->writeln('User successfully generated!');

    return Command::SUCCESS;
  }

  private function createUser($email, $password){
    $user = new User();
    $user->setRoles(["ROLE_ADMIN"]);
    $user->setEmail($email);
    $user->setFirstname("Admin");
    $user->setLastName("Ifox");
    $user->setPhone("+261340382332");
    $user->setPassword($this->passwordEncoder->encodePassword($user, $password));
    $this->entityManager->persist($user);
    $this->entityManager->flush();
  }
}