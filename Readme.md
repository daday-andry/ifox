# IFOX User management system documentation

**demo :**  [ifox.andry-nirina.dev](http://ifox.andry-nirina.com)

## 1. Modélisation

### 1.1 Diagramme de classe

L’image suivante décrit la structure de données du système et la relation entre eux.

![](https://lh4.googleusercontent.com/vLfpOxtNXJIhvyPUOiqEQ2mXDumLor_0nWl8RlPntwRxXK3KTaGLilK9CljjDNP_VDFgZxUQIgAVokkxvQranBdO0g8GsGF6aeE30kmcTQJgyPPEaEr8nkUjlu9v29sN_IROhFx3)

-   Un utilisateur (user) peut appartenir en aucun ou plusieur groupe (group)
    
-   Une groupe d’utilisateur peut contenir aucun ou plusieur utilisateurs

### 1.2 Customer journey
**Action** : Ajouter un utilisateur
**Acteur** : Admin
![](https://lh6.googleusercontent.com/24zLZ65rGzkT6aGd2Zat2JE9gQTmfGCypnUnZMzEhxipMHWuLoys4n2vFrqA3t0LHDIm1iShWmKPmnCcKKqKystKULQkSqyKhaKd2JWuHZvdzWBfZPRT1CoWz1Iyg5q6LYUeXM51)

  

  

**Action** : Ajouter une groupe d’utilisateur
**Acteur** : Admin

![](https://lh4.googleusercontent.com/3fz1jKtb92F-dEZN-GvY8W6UxTqaiyW7k0IwE-RReTvPoryHSRcO9Q6652qasurrZ7WZlABMxUYsllnLYoxN3hj5e_W8eN4xMF5W9MigQvRFeAmEdKLnz9UxSjzLqeQ2YCQHmikw)

  

**Action** : Assigner une groupe à un utilisateur

**Acteur** : Admin

![](https://lh3.googleusercontent.com/cnasgQ81bJ92HPlbDgAm6DuCiM-FREFY-7qno3yriSi_yNyR7WHdf0iMWlvFoK0-Hz8ti9ghuLc5N_xj45lUe5M10ua6hW9tbk80SogbhdBP1a5hys4iTZuIlKbXoUh6rN4KjuYo)

  
**Action** : retirer un utilisateur dans une groupe

**Acteur** : Admin

![](https://lh4.googleusercontent.com/PzWouYPgnp06KtqHlh6WgySFg3c_0Prk0Dof2ByaBa02Ia6ZoxUWiVbSxg3r6knwgkL_GtQ_SERo3yBlalS-F-8gOAPMGXfskHyoI8a2RFBg8NonwSnW_4k6Sx24E2SN5cnXX-Vt)


## 2. Technologie

 - Framework : Symfony 5.4 (LTS)
 - Database : Mysql
 - Environnement : Docker
 - Librairie front : Bootstrap
 - **Module et bundle :**

	-   Web pack encore pour la gestion des ressources (js, css, image)
	    
	-   FosRestBundle pour l’API
	    
	-   lexik/LexikJWTAuthenticationBundle pour la sécurisation de l’API
	    
	-   NelmioApiDocBundle pour la documentation de l’API
    

## 3. Installation et déploiement du projet

*Prérequis :*

-   npm ou yarn pour compiler les fichiers css et js (require node js)
    
-   composer pour la gestion des dépendances PHP
    
-   docker déjà configuré (optionnel)
    

  

### 3.1 Cloner le projet du repository et vous aurez un dossier ifox
La branche **main** contient le code source tandis que la branche dist continer des codes avec des css et js compiler. 

    git clone https://gitlab.com/daday-andry/ifox 

  

### 3.2 Installation du backend avec docker :

Ouvrir le dossier dans un terminal et exécuter les commandes suivantes :

    docker-compose up 
    docker exec -i -t ifox_php_1 composer install
    docker exec -i -t ifox_php_1 php bin/console d:s:u –force
    docker exec -i -t ifox_php_1 php bin/console lexik:jwt:generate-keypair

### 3.3 Installation sans docker
Si vous n’avez pas docker sur votre machine, vous aurez besoin d’installer un SGBD mysql si ce n’est pas encore fait.

**1 -** Modifier le DATABASE_URL dans le fichier .env en fonction de votre configuration

**2-** Ouvrir le dossier dans un terminal et exécuter les commandes suivantes :

    composer install
    php bin/console d:d:c
    php bin/console d:s:u
    php bin/console lexik:jwt:generate-keypair
  

### 3.4 : Générer un utilisateur (Admin) par défaut

**Lancer la commande :**

    php bin/console ifox:create:admin-user
  
Renseigner l'email du nouveau compte et le mot de passe. 

### 3.5 Compiler les fichier JS et css

Dans le dossier racine du projet, lancer la commande :

    yarn install
    yarn encore production

    

  

## 4.6 : Teste

Si vous avez utiliser docker, l’application et disponible sur [http://localhost:1080](http://localhost:1080)

Sinon, vous pouvez aussi lancer directement l’application avec la commande suivante :

    php -S localhost:1080 -t public


## 5. Spécification API Rest

Pour faciliter la communication avec d' autres interfaces, un api REST a été conçu.

Les different endpoints sont visible sur [https://ifox.andry-nirina.com](https://ifox.andry-nirina.com/api/doc)

  

Pour la sécurisation, l’utilisateur de l’API doit récupérer son token ( X-Auth-API ) en envoyant une requête POST vers [https://ifox.andry-nirina.com/api/login_check](https://ifox.andry-nirina.com/api/login_check) avec un username et passwod comme payload.

  

**Exemple json login pour :**

username : [admin@ifox.com](mailto:admin@ifox.com) ,

password : 4v3ry5tr0ngp4ssword

  

![](https://lh6.googleusercontent.com/i7XxvF0vPEp_4u73NwmNmMUnFrrKddk0q0U13dSepvUsky9hjJXeUDCJw3olCw7i5ikwlt-YBr6LcPxeot5gE6Opb91pkmc3oyldolPa8-mQYwPpZ53zCZMCO7VrP63ERCqKy26O)

  
  

On injecte le token dans le header de chaque requête comme suit :

Headername : X-Auth-API

Value : Bearer + token

  

![](https://lh3.googleusercontent.com/funoicIVzeL7U4SOu7IXetGLMe0lyRza2lu6lZ7L0vkU3MA1N71LFYZDXYOCJYl2PH1q3dGU6iOOd_FY1WVBbmp8JvEs-4GyTbqVv-E8wpZ4sM7XozxycFMz_fbxgYPof4scdwEX)

  
  
  
  

## 6. Information supplémentaire
**URL du plateforme** : [https://ifox.andry-nirina.com](https://ifox.andry-nirina.com)
**Accès admin :**

> 	email: [admin@ifox.com](mailto:admin@ifox.com) 	password :
> 4v3ry5tr0ngp4ssword

  

**Documentation de l’api** : [https://ifox.andry-nirina.com](https://ifox.andry-nirina.com/api/doc)
**API Login check** : [https://ifox.andry-nirina.com](https://ifox.andry-nirina.com/api/login_check)


